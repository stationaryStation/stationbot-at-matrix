const sdk = require('matrix-bot-sdk');
const MatrixClient = sdk.MatrixClient;
const SimpleFsStorageProvider = sdk.SimpleFsStorageProvider;
const AutojoinRoomsMixin = sdk.AutojoinRoomsMixin;

const homeserverUrl = "https://matrix.org";
const { token } = require('./config.json');

const storage = new SimpleFsStorageProvider('bot.json');

const client = new MatrixClient(homeserverUrl, token, storage);

AutojoinRoomsMixin.setupOnClient(client);

client.start().then(() => console.log('client is alive!!!'));
